#!/bin/sh

devices='amd64 armv7hf armv7hf-xbuild'

for device in $devices; do
	case "$device" in
		'amd64')
			baseImage='amd64/python'
			;;
		'armv7hf')
			baseImage='resin/armv7hf-python'
			;;
		'armv7hf-xbuild')
			baseImage='resin/armv7hf-python'
			resinXbuild=1
			;;
	esac
	sed -e "s@#{FROM}@${baseImage}:3@g" \
		-e "s@#{RESIN_XBUILD_START}@${resinXbuild+RUN [ \"cross-build-start\" ]}@g" \
		-e "s@#{RESIN_XBUILD_END}@${resinXbuild+RUN [ \"cross-build-end\" ]}@g" \
		Dockerfile.tpl > Dockerfile.${device}
done
