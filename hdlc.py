import serial.threaded


class HDLC_Packetizer(serial.threaded.Protocol):
    """
    Like serial.threaded.Packetizer with FLAG instead of TERMINATOR, but
    packets that did not begin with FLAG are thrown out
    """

    FLAG = b'\x7E'
    TIMEOUT = 1

    # Implementation borrowed from serial.threaded.Packetizer and
    # serial.threaded.FramedPacket
    def __init__(self):
        self.buffer = bytearray()
        self.transport = None
        self.in_packet = False

    def connection_made(self, transport):
        """Store transport"""
        self.transport = transport

    def connection_lost(self, exc):
        """Forget transport"""
        self.transport = None
        self.in_packet = False
        super().connection_lost(exc)

    def data_received(self, data):
        """Buffer received data, find FLAG, call handle_packet"""
        self.buffer.extend(data)
        while self.FLAG in self.buffer:
            packet, self.buffer = self.buffer.split(self.FLAG, 1)
            if packet:
                if self.in_packet:
                    self.handle_packet(packet)
                else:
                    self.handle_bad_packet(packet)
            self.in_packet = True

    def data_timeout(self, data):
        """Line timed out, packet no longer valid"""
        self.in_packet = False
        if self.buffer:
            self.handle_bad_packet(self.buffer)
            self.buffer.clear()

    def handle_packet(self, packet):
        """Process packets - to be overridden by subclassing"""
        raise NotImplementedError(
            'please implement functionality in handle_packet')

    def handle_bad_packet(self, packet):
        """Process invalid packets (packets without a start FLAG or without an
        end FLAG - timeout) - to be overridden by subclassing"""
        pass

    def write(self, data):
        # Sandwich self.FLAG on ends of stuffed message
        self.transport.write(data.join([self.FLAG] * 2))


class HDLC_Protocol(HDLC_Packetizer):
    ESCAPE = b'\x7D'
    ESCAPE_MASK = 0x20

    class InvalidConfigurationError(ValueError):
        pass

    class InvalidEscapeMaskError(InvalidConfigurationError):
        """Exception base class for bad ESCAPE_MASK configuration"""
        pass

    class ConflictingEscapedCharsError(InvalidConfigurationError):
        """Error: ESCAPED_CHARS contains duplicates"""
        pass

    class EscapeMaskZeroException(InvalidEscapeMaskError):
        """Error: ESCAPE_MASK cannot equal 0"""
        pass

    class EscapeMaskConflictException(InvalidEscapeMaskError):
        """A reserved flag with mask applied becomes a """
        pass

    def _check_args(self):
        """
        Check protocol configuration
        Call if you are changing ESCAPED_CHARS
        """
        if self.ESCAPE_MASK == 0:
            raise self.EscapeMaskZeroException()
        if len(self.ESCAPED_CHARS) != len(set(self.ESCAPED_CHARS)):
            raise self.ConflictingEscapedCharsError()
        for i, v in enumerate(self.ESCAPED_CHARS[:-1]):
            if bytes([v[0] ^ self.ESCAPE_MASK]) in self.ESCAPED_CHARS[i + 1:]:
                raise self.EscapeMaskConflictException()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ESCAPED_CHARS = [self.ESCAPE, self.FLAG]
        self._check_args()

    def _stuff(self, data):
        for ch in self.ESCAPED_CHARS:
            data = data.replace(
                ch, self.ESCAPE + bytes([ch[0] ^ self.ESCAPE_MASK]))
            # TODO: ord() or x[0] ?
        return data

    def _unstuff(self, data):
        for ch in self.ESCAPED_CHARS:
            data = data.replace(
                self.ESCAPE + bytes([ch[0] ^ self.ESCAPE_MASK]), ch)
            # TODO: ord() or x[0] ?
        return data

    def handle_packet(self, packet):
        self.handle_frame(self._unstuff(packet))

    def handle_frame(self, frame):
        """Process one frame - to be overridden by subclassing"""
        raise NotImplementedError(
            'please implement functionality in handle_frame')

    def write(self, data):
        super().write(self._stuff(data))
