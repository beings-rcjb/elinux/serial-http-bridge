FROM #{FROM}

#{RESIN_XBUILD_START}

RUN pip install pipenv

RUN mkdir -p /app
WORKDIR /app
COPY Pipfile Pipfile.lock ./

RUN pipenv install --three --system --deploy

COPY . .

ENV PORT 80
EXPOSE 80

ENTRYPOINT ["python", "serverapp.py"]
CMD []

#{RESIN_XBUILD_END}
