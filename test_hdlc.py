from hypothesis import given, settings, assume, reject
from hypothesis.strategies import binary
from pytest import fail

import serial
import ReaderThread
import hdlc
import threading


@given(binary(), binary(min_size=3, max_size=3))
@settings(max_examples=100)
def test_stuffing(data, _s_chars):
    flag = bytes([_s_chars[0]])
    escape = bytes([_s_chars[1]])
    escape_mask = _s_chars[2]

    class Proto(hdlc.HDLC_Protocol):
        FLAG = flag
        ESCAPE = escape
        ESCAPE_MASK = escape_mask

    try:
        p = Proto()
    except hdlc.HDLC_Protocol.InvalidConfigurationError:
        reject()
    else:
        assert data == p._unstuff(p._stuff(data))


@given(binary(min_size=1), binary(min_size=1, max_size=1))
@settings(max_examples=100)
def test_hdlc_packetizer(data, flag):
    flag = bytes([flag[0]])
    assume(flag not in data)
    ser = serial.serial_for_url('loop://', timeout=0.5)
    ev = threading.Event()

    class RecordFrames(hdlc.HDLC_Packetizer):
        FLAG = flag

        def handle_packet(self, packet):
            ev.set()
            self.packet = packet

    with ReaderThread.ReaderThread(ser, RecordFrames) as p:
        p.write(data)
        ev.wait()
        assert data == p.packet


@given(binary(min_size=1), binary(min_size=3, max_size=3))
@settings(max_examples=100)
def test_hdlc_protocol(data, _s_chars):
    flag = bytes([_s_chars[0]])
    escape = bytes([_s_chars[1]])
    escape_mask = _s_chars[2]

    ser = serial.serial_for_url('loop://', timeout=0.5)
    ev = threading.Event()

    class RecordFrames(hdlc.HDLC_Protocol):
        FLAG = flag
        ESCAPE = escape
        ESCAPE_MASK = escape_mask

        def handle_frame(self, packet):
            ev.set()
            self.packet = packet

        def connection_lost(self, exc):
            if exc:
                fail(exc)

    try:
        with ReaderThread.ReaderThread(ser, RecordFrames) as p:
            p.write(data)
            ev.wait()
            assert data == p.packet
    except hdlc.HDLC_Protocol.InvalidConfigurationError:
        reject()

# TODO: test timeouts and data without leading flag
