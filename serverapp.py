from serial import serial_for_url
from hdlc import HDLC_Protocol
from flask import Flask, request, abort, make_response
from functools import wraps
import requests
import concurrent.futures as futures

app = Flask(__name__)

handler_map = {}
g_protocol = None


def __get_get_handler_id():
    curr_id = 0

    def get_handler_id():
        nonlocal curr_id
        tmp = curr_id
        curr_id += 1
        return tmp
    return get_handler_id


get_handler_id = __get_get_handler_id()


def limit_content_length(max_length):  # https://stackoverflow.com/a/25036561
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            cl = request.content_length
            if cl is not None and cl > max_length:
                abort(413)
            return f(*args, **kwargs)
        return wrapper
    return decorator


# TODO: Text error handlers

@app.route('/register/<int:frame_id>', methods=['POST'])
@limit_content_length(16 * 1024)
def handler_register(frame_id):
    handler_url = request.form['url']
    if frame_id < 0 or frame_id > 255:
        abort(400)
    if not handler_url:
        abort(400)
    if frame_id in handler_map:
        abort(409)  # Conflict
    handler_map[frame_id] = request.form['url']
    return make_response('', 201)


@app.route('/unregister/<int:frame_id>', methods=['POST'])
@limit_content_length(16 * 1024)
def handler_unregister(frame_id):
    handler_url = request.form['url']
    if frame_id < 0 or frame_id > 255:
        abort(400)
    if not handler_url:
        abort(400)
    try:
        if handler_map[frame_id] != handler_url:
            abort(404)
    except KeyError:
        abort(404)
    del handler_map[frame_id]
    return make_response('', 200)


@app.route('/send/<int:frame_id>', methods=['POST'])
def send_data(frame_id):
    if frame_id < 0 or frame_id > 255:
        abort(400)
    if g_protocol is None:
        abort(503)  # Temporarily unavailable
    g_protocol.write(bytes([frame_id]) + bytes(request.data))
    return make_response('', 200)


class ReaderDispatcher(HDLC_Protocol):

    def __init__(self, executor, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.senderExecutor = executor

    def dispatch(self, url, data):
        requests.post(url, data,
                      headers={'Content-Type': 'application/octet-stream'})

    def handle_frame(self, frame):
        frame_id = frame[0]
        try:
            url = handler_map[frame_id]
            self.senderExecutor.submit(self.dispatch, url, frame[1:])
        except KeyError:
            pass


if __name__ == '__main__':
    import os
    from argparse import ArgumentParser
    from ReaderThread import ReaderThread
    parser = ArgumentParser(description="")  # TODO: Description
    parser.add_argument('-d', '--serial-device', type=str,
                        required=True,
                        help="Serial device to use")
    parser.add_argument('-b', '--baud', type=int,
                        default=115200,
                        help="Port baud rate")
    parser.add_argument('-t', '--timeout', type=float,
                        default=0.1,
                        help="Protocol frame timeout in seconds")
    parser.add_argument('-p', '--port', type=int,
                        default=int(os.environ.get('PORT', 8080)),
                        help="Web server port")

    args = parser.parse_args()
    with serial_for_url(args.serial_device) as ser:
        ser.baudrate = args.baud
        ser.timeout = args.timeout
        with futures.ThreadPoolExecutor(max_workers=2) as executor:
            with ReaderThread(ser, lambda: ReaderDispatcher(executor))\
                    as protocol:
                g_protocol = protocol
                app.run(host='0.0.0.0', port=args.port)
    g_protocol = None
